import java.util.Random;

/**
 * Thread che simula la richiesta di risorse a intervalli casuali.
 */
public class ResourceTakingSimulatorThread extends Thread {
	/** Massimo intervallo fra successive richieste in millisecondi */
	private static final int MAX_SLEEP = 10000;

	/** Massimo numero di risorse richieste dopo ciascun intervallo */
	private static final int MAX_TAKEN_RESOURCES = 80;

	/** Generatore di numeri casuali */
	private static final Random RANDOM = new Random();

	/** Client che effettua le richieste di risorse */
	private final Client client;

	/**
	 * Costruttore di default.
	 * 
	 * @param client
	 *            il client che effettua le richieste di risorse
	 */
	public ResourceTakingSimulatorThread(Client client) {
		this.client = client;
		setName("take-simulator");
	}

	/**
	 * Metodo principale del thread. Simula la richiesta di risorse a intervalli
	 * casuali.
	 * <p>
	 * Il metodo è costituito da un ciclo infinito. All'inizio di ogni
	 * iterazione si attende un intervallo di tempo casuale distribuito
	 * uniformemente fra 0 e {@link #MAX_SLEEP} millisecondi. Finito il tempo di
	 * attesa, viene richiesto un numero casuale di risorse scelto uniformemente
	 * fra 1 e {@link #MAX_TAKEN_RESOURCES}.
	 * <p>
	 * La richiesta di risorse è effettuata invocando il metodo
	 * {@link Client#take(int)}.
	 */
	public void run() {
		for (;;) {
			try {
				Thread.sleep((long) (RANDOM.nextDouble() * ResourceTakingSimulatorThread.MAX_SLEEP));
			} catch (InterruptedException e) {
			}

			int resourcesToTake = RANDOM.nextInt(MAX_TAKEN_RESOURCES) + 1;

			client.take(resourcesToTake);
		}
	}
}