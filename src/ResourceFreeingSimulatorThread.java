import java.util.Random;

/**
 * Thread che simula la liberazione di risorse a intervalli casuali.
 */
public class ResourceFreeingSimulatorThread extends Thread {
	/** Massimo intervallo fra successive liberazioni in millisecondi */
	private static final long MAX_SLEEP = 10000;

	/** Generatore di numeri casuali */
	private static final Random RANDOM = new Random();

	/** Client che libera risorse */
	private final Client client;

	/**
	 * Costruttore di default.
	 * 
	 * @param client
	 *            il client che libera risorse
	 */
	public ResourceFreeingSimulatorThread(Client client) {
		this.client = client;
		setName("give-simulator");
	}

	/**
	 * Metodo principale del thread. Simula la liberazione di risorse a
	 * intervalli casuali.
	 * <p>
	 * Il metodo è costituito da un ciclo infinito. All'inizio di ogni
	 * iterazione si attende un intervallo di tempo casuale distribuito
	 * uniformemente fra 0 e {@link #MAX_SLEEP} millisecondi. Finito il tempo di
	 * attesa, se ci sono risorse allocate, ne vengono liberate una quantità
	 * casuale scelta in modo uniforme tra 1 e il numero totale di risorse
	 * allocate.
	 * <p>
	 * La liberazione di risorse è effettuata invocando il metodo
	 * {@link Client#give(int)}.
	 */
	public void run() {
		for (;;) {
			try {
				Thread.sleep((long) (RANDOM.nextDouble() * MAX_SLEEP));
			} catch (InterruptedException e) {
			}

			int busyResources = client.getTotalResources()
					- client.getAvailableResources();

			if (busyResources == 0) {
				continue;
			}

			int resourcesToGive = RANDOM.nextInt(busyResources) + 1;

			client.give(resourcesToGive);
		}
	}
}