import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;

/**
 * Implementazione dell'interfaccia {@link Broker}.
 * <p>
 * Questa classe è solamente il frontend del servizio. La gestione
 * dell'inventario delle risorse disponibili è delegata a un'istanza della
 * classe {@link Inventory}. L'inoltro delle richieste <i>reserve</i> ai client
 * è delegata a un'istanza della classe {@link Reserver}.
 */
public class BrokerImpl extends UnicastRemoteObject implements Broker {
	private static final long serialVersionUID = 1L;

	/** Inventario delle risorse disponibili su ciascun client */
	private Inventory inventory = new Inventory();

	/** Inoltra le richieste <i>reserve</i> ai client */
	private Reserver reserver = new Reserver();

	/**
	 * Costruttore di default.
	 */
	protected BrokerImpl() throws RemoteException {
		super();
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * L'operazione è effettuata richiedendo all'inventario di allocare il
	 * numero di risorse specificato (tramite il metodo
	 * {@link Inventory#remove(int)}). Se le risorse sono disponibili, esse
	 * vengono effettivamente riservate sui client (tramite una chiamata a
	 * {@link Reserver#reserveAll(Map)}), e questo metodo restituisce
	 * {@code true}. Altrimenti, {@link Inventory#remove(int)} lancia
	 * l'eccezione {@link InsufficientResourcesException}, che viene catturata,
	 * e il metodo restituisce {@code false}.
	 */
	@Override
	public boolean take(final String user, final int resources) {
		System.err.println(String.format(
				"Asked to take %d resources from user %s", resources, user));

		try {
			final Map<String, Integer> takenResourcesByClient = inventory
					.remove(resources);
			System.err.println(String.format(
					"Reserving %d resources for user %s", resources, user));
			reserver.reserveAll(takenResourcesByClient);
			return true;
		} catch (InsufficientResourcesException e) {
			System.err.println(String.format(
					"%d resources are NOT available for user %s", resources,
					user));
			return false;
		} finally {
			inventory.dump();
		}
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * L'operazione è implementata inoltrando la richiesta all'inventario,
	 * tramite il metodo {@link Inventory#add(String, int)}.
	 */
	@Override
	public void give(final String user, final int resources)
			throws RemoteException {
		System.err.println(String.format("User %s gives us %d resources", user,
				resources));

		inventory.add(user, resources);
		inventory.dump();
	}
}
