import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interfaccia remota che consente ai client di liberare e allocare risorse.
 */
public interface Broker extends Remote {
	/**
	 * Prova ad allocare un dato numero di risorse per un client.
	 * 
	 * @param user
	 *            il client che vuole allocare risorse
	 * @param resources
	 *            il numero di risorse che si desidera allocare
	 * 
	 * @return {@code true} se è stato possibile allocare le risorse richieste,
	 *         {@code false} altrimenti.
	 * @throws RemoteException
	 */
	boolean take(String user, int resources) throws RemoteException;

	/**
	 * Indica che su un client ci sono nuove risorse libere disponibili.
	 * 
	 * @param user
	 *            il client le cui risorse disponibili sono aumentate
	 * @param resources
	 *            il numero di nuove risorse disponibili
	 * @throws RemoteException
	 */
	void give(String user, int resources) throws RemoteException;
}