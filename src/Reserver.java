import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Inoltra le richieste <i>reserve</i> ai client.
 */
public class Reserver {
	/** Porta su cui sono aperte le connessioni per le operazioni <i>reserve</i> */
	private static final int RESERVER_PORT = 1112;

	/**
	 * Richiede a uno o più client di riservare ciascuno un dato numero di
	 * risorse.
	 * 
	 * @param resourcesToReserveByClient
	 *            una mappa che associa a uno o più client il numero (positivo)
	 *            di risorse da riservare su quel client
	 * @see #reserve(String, int)
	 */
	public void reserveAll(final Map<String, Integer> resourcesToReserveByClient) {
		try {
			for (Entry<String, Integer> clientResourcesToReserve : resourcesToReserveByClient
					.entrySet()) {
				reserve(clientResourcesToReserve.getKey(),
						clientResourcesToReserve.getValue());
			}
		} catch (UnknownHostException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Richiede a un dato client di riservare un dato numero di risorse.
	 * <p>
	 * L'inoltro di una richiesta <i>reserve</i> è effettuato nel modo seguente:
	 * <ol>
	 * <li>si apre un {@link Socket} verso l'indirizzo contenuto in {@code user}
	 * , sulla porta {@link #RESERVER_PORT}.
	 * <li>usando un {@link OutputStreamWriter} e un {@link BufferedWriter} si
	 * scrive sulla connessione il numero di risorse da riservare, come numero
	 * in notazione decimale, codificato in ASCII, seguito da un <i>newline</i>.
	 * <li>si fa un <i>flush</i> del <i>writer</i> e si chiude il socket.
	 * </ol>
	 * 
	 * @param user
	 *            il client a cui inoltrare la richiesta
	 * @param resourcesToReserve
	 *            il numero di risorse di reservare sul client
	 * @throws IOException
	 *             se l'operazione fallisce per un errore di IO
	 */
	private void reserve(final String user, final int resourcesToReserve)
			throws IOException {
		if (resourcesToReserve == 0) {
			return;
		}

		System.err.println(String.format(
				"Reserving %d resources from user %s... ", resourcesToReserve,
				user));

		final Socket socket = new Socket(user, RESERVER_PORT);
		try {
			final OutputStream outputStream = socket.getOutputStream();
			final BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(outputStream));

			writer.write(Integer.toString(resourcesToReserve));
			writer.newLine();
			writer.flush();
		} finally {
			socket.close();
		}

		System.err.println(String.format(
				"Reserving %d resources from user %s... done!",
				resourcesToReserve, user));
	}

}
