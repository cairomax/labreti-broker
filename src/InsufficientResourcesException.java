/**
 * Eccezione lanciata quando non sono disponibili risorse a sufficienza per
 * soddisfare una richiesta.
 */
public class InsufficientResourcesException extends Exception {
	private static final long serialVersionUID = 1L;
}
