import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Classe eseguibile che avvia il broker e lo mette in ascolto di richieste RMI.
 */
public class Server {
	/** Porta su cui accettare connessioni Java RMI del client */
	private static final int BROKER_PORT = 1111;

	public static void main(String[] args) throws RemoteException {
		final BrokerImpl broker = new BrokerImpl();
		final Registry registry = LocateRegistry.createRegistry(BROKER_PORT);
		registry.rebind("ResourceBroker", broker);

		System.err.println("Server ready");
	}
}
