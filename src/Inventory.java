import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Un inventario che tiene traccia delle risorse disponibili su ciascun client.
 * <p>
 * Supporta due operazioni per modificare il contenuto dell'inventario:
 * <ul>
 * <li>{@link #add(String, int)} incrementa le risorse disponibili su un client
 * <li>{@link #remove(int)} rimuove un dato numero di risorse dall'inventario,
 * togliendole da uno o più client
 * </ul>
 * <p>
 * Questa classe è thread-safe. L'accesso concorrente è protetto da un lock
 * sull'istanza principale (mediante metodi {@code synchronized}).
 * <p>
 * L'inventario è implementato tramite una {@link HashMap} che associa a ciascun
 * client il numero di risorse disponibili su quel client (
 * {@link #availableResourcesByClient}) e un contatore che tiene traccia del
 * numero totale di risorse disponibili ({@link #totalAvailableResources}).
 * Questi campi vengono letti e aggiornati contemporaneamente all'interno di
 * sezioni critiche protette dal lock, pertanto nessun thread può vedere uno
 * stato incoerente.
 */
public class Inventory {
	/**
	 * Numero totale di risorse disponibili.
	 */
	private int totalAvailableResources = 0;

	/**
	 * Risorse disponibili su ciascun client. Se un client non ha risorse
	 * libere, la mappa può non avere un'entrata per quel client.
	 */
	private HashMap<String, Integer> availableResourcesByClient = new HashMap<String, Integer>();

	/**
	 * Rimuove un dato numero di risorse dall'inventario, decrementando la
	 * disponibilità di risorse di uno o più client.
	 * <p>
	 * Questo metodo controlla che il numero totale di risorse disponibili (
	 * {@link #totalAvailableResources}) sia sufficiente a soddisfare la
	 * richiesta. Nel caso ciò non sia possibile, lancia subito un'eccezione,
	 * altrimenti provvede ad allocare le risorse seguendo il seguente
	 * algoritmo.
	 * <p>
	 * Per allocare le risorse richieste, questo metodo itera su tutti i client
	 * in un ordine non definito. Per ogni client, vengono rimosse
	 * dall'inventario tutte le risorse a disposizione del client necessarie a
	 * completare la richiesta. Nel caso in cui le risorse a disposizione del
	 * client non siano sufficienti a soddisfare la richiesta, si prendono
	 * comunque tutte (lasciando il client a 0) e si passa al client successivo.
	 * <p>
	 * Questo metodo restituisce il numero di risorse prese da ciascun client,
	 * in modo che il chiamante possa effettivamente riservare le risorse.
	 * 
	 * @param resources
	 *            il numero (positivo) di risorse da rimuovere
	 * @return una mappa che ha come chiavi i client a cui sono state prese una
	 *         o più risorse, e come valori il numero di risorse prese da
	 *         ciascuno.
	 * @throws InsufficientResourcesException
	 *             se le risorse nell'inventario non sono sufficienti a
	 *             soddisfare la richiesta
	 * @throws IllegalArgumentException
	 *             se {@code resources <= 0}.
	 */
	public synchronized Map<String, Integer> remove(final int resources)
			throws InsufficientResourcesException {
		if (resources <= 0) {
			throw new IllegalArgumentException("Taken resources must be > 0");
		}

		if (resources > totalAvailableResources) {
			throw new InsufficientResourcesException();
		}

		final HashMap<String, Integer> resourcesToReserveByClient = new HashMap<String, Integer>();

		int remainingResourcesToReserve = resources;
		for (Entry<String, Integer> clientAvailableResources : availableResourcesByClient
				.entrySet()) {
			final String client = clientAvailableResources.getKey();
			final int availableResources = clientAvailableResources.getValue();

			int resourcesToReserve = Math.min(availableResources,
					remainingResourcesToReserve);

			if (resourcesToReserve == 0) {
				continue;
			}

			remainingResourcesToReserve -= resourcesToReserve;
			resourcesToReserveByClient.put(client, resourcesToReserve);

			clientAvailableResources.setValue(availableResources
					- resourcesToReserve);

			System.err.println(String.format(
					"Taking %d resource from client %s (who had %d)",
					resourcesToReserve, client, availableResources));
		}

		totalAvailableResources -= resources;
		return resourcesToReserveByClient;
	}

	/**
	 * Incrementa di un dato valore la disponibilità di risorse di un client.
	 * 
	 * @param user
	 *            il client di cui incrementare il numero di risorse
	 * @param newAvailableResources
	 *            il valore di cui incrementare la disponibilità di risorse del
	 *            client
	 */
	public synchronized void add(final String user,
			final int newAvailableResources) {
		Integer availableResources = availableResourcesByClient.get(user);

		if (availableResources == null) {
			availableResources = 0;
		}

		availableResourcesByClient.put(user, availableResources
				+ newAvailableResources);
		totalAvailableResources += newAvailableResources;
	}

	/**
	 * Stampa su <i>stdout</i> il contenuto dell'inventario.
	 */
	public synchronized void dump() {
		System.out.println("Inventory: " + availableResourcesByClient
				+ " (total: " + totalAvailableResources + ")");
	}
}