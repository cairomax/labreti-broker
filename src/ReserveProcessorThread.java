import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Thread che processa le richieste di riservare risorse sul client.
 * <p>
 * La ricezione delle richieste <i>reserve</i> è effettuata da un thread singolo
 * che accetta e processa tutte le connessioni in ingresso.
 * 
 * @see #run()
 */
class ReserveProcessorThread extends Thread {
	/** Porta su cui ascoltare delle richieste <i>reserve</i> */
	private static final int RESERVER_PORT = 1112;

	/** Client su cui vengono riservate le risorse */
	private final Client client;

	/**
	 * Costruttore di default.
	 * 
	 * @param client
	 *            il client su cui vengono riservate le risorse
	 */
	public ReserveProcessorThread(Client client) {
		this.client = client;
		this.setName("reserve-processor");
	}

	/**
	 * Metodo principale del thread. Accetta connessioni dal server e processa
	 * le relative richieste <i>reserve<i>.
	 * <p>
	 * All'avvio, apre un {@link Socket} in ascolto sulla porta
	 * {@link #RESERVER_PORT} ed entra in un ciclo infinito in cui accetta nuove
	 * connessioni in ingresso. Ad ogni nuova connessione accettata, il metodo
	 * legge la prima riga ricevuta nello stream (tramite un
	 * {@link InputStreamReader} e un {@link BufferedReader}). La riga viene
	 * interpretata come un numero in notazione decimale (in <i>ASCII</i>) che
	 * corrisponde al numero di risorse da riservare sul client.
	 * <p>
	 * La richiesta di riservare risorse è inoltrata al {@link #client} tramite
	 * il metodo {@link Client#reserve(int)}.
	 */
	@Override
	public void run() {
		try {
			processReserveRequests();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (InsufficientResourcesException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Accetta connessioni dal server e processa le relative richieste
	 * <i>reserve<i>.
	 * <p>
	 * Questo metodo implementa internamente quanto specificato dal metodo
	 * {@link #run()}, ma può lanciare eccezioni.
	 * 
	 * @throws IOException
	 *             se qualche operazione fallisce per un errore di IO
	 * @throws InsufficientResourcesException
	 *             se viene ricevuta una richiesta di riservare più delle
	 *             risorse disponibili
	 */
	private void processReserveRequests() throws IOException,
			InsufficientResourcesException {
		final ServerSocket reserveSocket = new ServerSocket(
				ReserveProcessorThread.RESERVER_PORT);

		try {
			for (;;) {
				final Socket socket = reserveSocket.accept();
				try {
					final InputStream inputStream = socket.getInputStream();
					final BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputStream));
					final String line = reader.readLine();
					final int resourcesToReserve = Integer.valueOf(line);

					client.reserve(resourcesToReserve);
				} finally {
					socket.close();
				}
			}
		} finally {
			reserveSocket.close();
		}
	}
}